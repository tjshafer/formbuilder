<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('social_logins', function (Blueprint $table) {
            $table->id();
            $table->string('social_type')->nullable();
            $table->string('social_id')->nullable();
            $table->bigInteger('user_id');
            $table->timestamps();
        });
    }
};
