<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('forms', function (Blueprint $table) {
            $table->string('logo')->after('title')->nullable();
            $table->string('email')->after('logo')->nullable();
            $table->text('success_msg')->after('email')->nullable();
        });
    }
};
