<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('form_values', function (Blueprint $table) {
            $table->float('amount')->after('json')->nullable();
            $table->string('currency_symbol')->after('amount')->nullable();
            $table->string('currency_name')->after('currency_symbol')->nullable();
            $table->string('transaction_id')->after('currency_name')->nullable();
        });
    }
};
