<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class UtilityFacadesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        App::bind('utility', fn () => new \App\Facades\Utility);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
