<?php

namespace App\Http\Controllers;

use App\DataTables\RolesDataTable;
use App\Models\Module;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:manage-role|create-role|edit-role|delete-role', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-role', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-role', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-role', ['only' => ['destroy']]);
    }

    public function index(RolesDataTable $dataTable)
    {
        return $dataTable->render('roles.index');
    }

    public function create(): array
    {
        $permission = Permission::get();
        $view = view('roles.create', ['permission' => $permission]);

        return ['html' => $view->render()];
    }

    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        request()->validate([
            'name' => 'required',
        ]);
        $role = Role::create(['name' => $request->input('name')]);

        return to_route('roles.index')
                        ->with('success', __('Role Added successfully.'));
    }

    public function show($id): \Illuminate\Contracts\View\View
    {
        $role = Role::find($id);
        $permissions = $role->permissions->pluck('name', 'id')->toArray();
        $allpermissions = Permission::all()->pluck('name', 'id')->toArray();
        $allmodules = Module::all()->pluck('name', 'id')->toArray();

        return view('roles.show')
            ->with('role', $role)
            ->with('permissions', $permissions)
            ->with('allpermissions', $allpermissions)
            ->with('allmodules', $allmodules);
    }

    public function edit($id): array
    {
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table('role_has_permissions')->where('role_has_permissions.role_id', $id)
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();
        $view = view('roles.edit', [
            'role' => $role,
            'permission' => $permission,
            'rolePermissions' => $rolePermissions,
        ]);

        return ['html' => $view->render()];
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();
        $role->syncPermissions($request->input('permission'));

        return to_route('roles.index')
                        ->with('success', __('Role updated successfully'));
    }

    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        $role = Role::find($id);
        $role->delete();

        return to_route('roles.index')
                        ->with('success', __('Role deleted successfully'));
    }

    public function assignPermission(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        $role = Role::find($id);
        $permissions = $role->permissions()->get();
        $role->revokePermissionTo($permissions);
        $role->givePermissionTo($request->permissions);

        return to_route('roles.index')->with('success', __('Permissions assigned to Role successfully'));
    }
}
