<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\Models\SocialLogin;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:manage-user|create-user|edit-user|delete-user', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-user', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-user', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-user', ['only' => ['destroy']]);
    }

    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('users.index');
    }

    public function create(): array
    {
        $roles = Role::pluck('name', 'name')->all();
        $view = view('users.create', ['roles' => $roles]);

        return ['html' => $view->render()];
    }

    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required',
        ]);
        $input = $request->all();
        $input['type'] = $input['roles'];
        $input['password'] = Hash::make($input['password']);
        $input['lang'] = setting('default_language');
        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        $message = 'Welcome'.config('app.name').'<br/>';
        $message .= "
        <b>Dear </b> $request->name <br/>
        <b>You are added in our app
        <p> Your login Details:</p>
        </b> $request->email<br/>";

        return to_route('users.index')
            ->with('success', __('User Created successfully.'));
    }

    public function edit($id): array
    {
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();
        $view = view('users.edit', [
            'user' => $user,
            'roles' => $roles,
            'userRole' => $userRole,
        ]);

        return ['html' => $view->render()];
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'same:confirm-password',
            'roles' => 'required',
        ]);
        $input = $request->all();
        if (! isset($input['password']) || $input['password'] != '') {
            $input['password'] = Hash::make($input['password']);
        } else {
            unset($input['password']);
        }
        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($request->input('roles'));

        return to_route('users.index')
            ->with('success', __('User updated successfully'));
    }

    public function update_profile(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
        ]);
        $input = $request->all();
        $user = User::find($id);
        $this->validate($request, ['profile' => 'required|image']);
        if ($request->hasFile('profile')) {
            $filename = $request->file('profile')->getClientOriginalName();
            $request->file('profile')->storeAs('profile', $filename);
            $input['profile'] = $filename;
        }
        $user->update($input);

        return to_route('user.profile', $id)
            ->with('success', __('Profile update successfully'));
    }

    public function destroy($id)
    {
        if ($id != 1) {
            $user = User::find($id);
            $social_login = SocialLogin::where('user_id', $id)->get();

            foreach ($social_login as $value) {
                if ($user->type == 'Admin') {
                    continue;
                }
                if (!$value) {
                    continue;
                }
                $value->delete();
            }
            $user->delete();

            return redirect()->back()->with('success', __('User deleted successfully'));
        }
        return redirect()->back()->with('failed', __('Permission Denied.'));
    }

    public function accountStatus($id)
    {
        $user = User::find($id);
        // dd($id);
        if ($user->active_status == 1) {
            $user->active_status = 0;
            $user->save();

            return redirect()->back()->with('success', 'User Deactiveted Successfully');
        }
        $user->active_status = 1;
        $user->save();
        return redirect()->back()->with('success', 'User Activeted Successfully');
    }

    public function profile($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $user = User::find($id);

        return  view('users.profile', ['user' => $user]);
    }
}
