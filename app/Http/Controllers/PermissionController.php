<?php

namespace App\Http\Controllers;

use App\DataTables\PermissionsDataTable;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:manage-permission|create-permission|edit-permission|delete-permission', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-permission', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit-permission', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete-permission', ['only' => ['destroy']]);
    }

    public function index(PermissionsDataTable $dataTable)
    {
        return $dataTable->render('permission.index');
    }

    public function create(): array
    {
        $view = view('permission.create');

        return ['html' => $view->render()];
    }

    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        request()->validate([
            'name' => 'required',
        ]);
        Permission::create($request->all());

        return to_route('permission.index')
            ->with('success', __('Permission Added successfully.'));
    }

    public function edit($id): array
    {
        $permission = Permission::find($id);
        $view = view('permission.edit')->with('permission', $permission);

        return ['html' => $view->render()];
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $permission = Permission::find($id);
        $permission->update($request->except('_token'));

        return to_route('permission.index')
            ->with('success', __('Permission updated successfully'));
    }

    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        DB::table('role_has_permissions')->where('permission_id', $id)->delete();
        $permission = Permission::find($id);
        $permission->delete();

        return to_route('permission.index')
            ->with('success', __('Permission Delete successfully'));
    }
}
