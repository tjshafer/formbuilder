<?php

namespace App\Http\Controllers;

use App\DataTables\ModuleDataTable;
use App\Models\Module;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class ModuleController extends Controller
{
    public function index(ModuleDataTable $dataTable)
    {
        return $dataTable->render('module.index');
    }

    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('module.create');
    }

    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $this->validate($request, [
            'name' => 'required|regex:/^[a-zA-Z0-9\-_\.]+$/|min:4|unique:permissions',
        ], [
            'regex' => 'Invalid Entry! Only letters,underscores,hypens and numbers are allowed',
        ]);
        $this->module = module::create([
            'name' => str_replace(' ', '-', strtolower($request->name)),
        ]);
        $module_name = str_replace(' ', '-', strtolower($request->name));
        if (! empty($_POST['permissions'])) {
            foreach ($_POST['permissions'] as $check) {
                if ($check == 'M') {
                    $data[] = ['name' => 'manage-'.$module_name, 'guard_name' => 'web', 'created_at' => new \DateTime()];
                } elseif ($check == 'C') {
                    $data[] = ['name' => 'create-'.$module_name, 'guard_name' => 'web', 'created_at' => new \DateTime()];
                } elseif ($check == 'E') {
                    $data[] = ['name' => 'edit-'.$module_name, 'guard_name' => 'web', 'created_at' => new \DateTime()];
                } elseif ($check == 'D') {
                    $data[] = ['name' => 'delete-'.$module_name, 'guard_name' => 'web', 'created_at' => new \DateTime()];
                } elseif ($check == 'S') {
                    $data[] = ['name' => 'show-'.$module_name, 'guard_name' => 'web', 'created_at' => new \DateTime()];
                }
            }
        }
        Permission::insert($data);

        return to_route('module.index')->with('success', __('Module Created Successfully'));
    }

    public function edit($id): \Illuminate\Contracts\View\View
    {
        $this->module = module::findOrfail($id);

        return view('module.edit')->with('module', $this->module);
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $modules = Module::find($id);
        $this->validate($request, [
            'name' => 'required|regex:/^[a-zA-Z0-9\-_\.]+$/|min:4|unique:modules,name,'.$modules->id,
        ], [
            'regex' => __('Invalid Entry! Only letters,underscores,hypens and numbers are allowed'),
        ]);

        $modules->name = str_replace(' ', '-', strtolower($request->name));
        $permissions = DB::table('permissions')
            ->where('name', 'like', '%'.$request->old_name.'%')
            ->get();
        $module_name = str_replace(' ', '-', strtolower($request->name));
        foreach ($permissions as $permission) {
            $update_permission = permission::find($permission->id);
            if ($permission->name == 'manage-'.$request->old_name) {
                $update_permission->name = 'manage-'.$module_name;
            }
            if ($permission->name == 'create-'.$request->old_name) {
                $update_permission->name = 'create-'.$module_name;
            }
            if ($permission->name == 'edit-'.$request->old_name) {
                $update_permission->name = 'edit-'.$module_name;
            }
            if ($permission->name == 'delete-'.$request->old_name) {
                $update_permission->name = 'delete-'.$module_name;
            }
            if ($permission->name == 'show-'.$request->old_name) {
                $update_permission->name = 'show-'.$module_name;
            }
            $update_permission->save();
        }
        $modules->save();

        return to_route('module.index')->with('success', __('Module Updated Sucessfully'));
    }

    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        $this->module = module::find($id);
        $users = DB::table('permissions')
            ->where('name', 'like', '%'.$this->module->name.'%')
            ->get();
        foreach ($users as $user) {
            $permission = permission::find($user->id);

            $permission->delete();
        }
        $this->module->delete();

        return to_route('module.index')->with('success', __('Module Deleted Successfully'));
    }
}
