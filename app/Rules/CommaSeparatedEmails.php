<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class CommaSeparatedEmails implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        $value = implode(',', $value);
        // dd($value);

        return ! Validator::make(
            [
                "{$attribute}" => explode(',', $value),
            ],
            [
                "{$attribute}.*" => 'required|email',
            ]
        )->fails();
    }

    /**
     * Get the validation error message.
     */
    public function message(): string
    {
        return 'The :attribute must have valid email addresses.';
    }
}
